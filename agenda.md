Agenda Private - with Presenters contacts

| Time        | Duration     | Topic                                                                                                                              | Presenter                               |
|-------------|--------------|------------------------------------------------------------------------------------------------------------------------------------|-----------------------------------------|
| **08:30** |              | **Morning Coffee**                                                                                                               |                                         |
|             |              | **Meet the Automotive@Eclipse community**                                                                                        |                                         |
| **09:00** | **10 min** | **Welcome, Agenda**                                                                                                              | Andy (Bosch)                      |
| 09:10 | 10 min | Automotive TLP                                                                      | Harald, Andy (Bosch) |
|       |        | - PMC                                                                              |                            |
|       |        | - Project Overview, new Project Proposals in 2022 (short overview of the projects) |                            |
|       |        | - Landing Page (if available)                                                     |                            |
| 09:20       | 20 min       | openPASS                                                                                                                           | Arun Das (BMW), Abhijit Thorat (Ansys)  |
| 09:40       | 10 min       | Future contribution of Ansys to openPass                                                                                           | Arun Das (BMW), Abhijit Thorat (Ansys)  |
| 09:50       | 20 min       | openMobility                                                                                                                       | Robert (DLR)                            |
| 10:10       | 20 min       | openMDM                                                                                                                            | Hans-Dirk (Karakun)                     |
|             |              | **News from Automotive**                                                                                                         |                                         |
| 10:30       | 30 min       | Catena-X - Big Picture and Status                                                                                                  | Roman Werner (ZF),  Daniel Miehle (BMW)             |
| **11:00** | **15 min** | **- - - B R E A K - - -**                                                                                                        |                                         |
| 11:15       | 30 min       | Catena-X - Technical Deep Dive into                                                                                  | Maximilian Ong (Mercedes Benz)             |
| 11:45       | 20 min       | Mobility Operations (MobilityOps)                                                                                            | Martin Reuss (7p)                                |
| 12:05       | 25 min       | openPASS - Stochastic Cognitive Model (SCM) for driver behavior                                                                    | Arun Das (BMW), Dominik Jantschar (BMW) |
| **12:30** | **90 min** | **--- Lunch ---**                                                                                                              |                                         |
|             |              | **News from Software-defined Vehicle (SDV)**                                                                                     |                                         |
| 14:00       | 15 min       | SDV Working Group                                                                                                                  | Sara Gallian - tbc                      |
| 14:15       | 15 min       | Eclipse Adore                                                                                                                | Matthias Nichting                      |
| 14:30       | 15 min       | New SDV Projects (1): Eclipse openDuT                                                                                                                | Thomas Irmscher (ETAS), Johannes Baumgartl (Mercedes Benz Inno Tech), Oliver Hartkopp (VW), Juergen Wurzinger (AVL) |
| 14:45       | 15 min       | tbd                                                                                                                                | tbd                                     |
| 15:00       | 15 min       | tbd                                                                                                                                | tbd                                     |
| 15:15       | 15 min       | tbd                                                                                                                                | tbd                                     |
| 15:30       | 15 min       | tbd                                                                                                                                | tbd                                     |
| **15:45** | **30 min** | **- - - B R E A K - - -**                                                                                                        |                                         |
| 16:15       | 15 min       | tbd                                                                                                                                | tbd                                     |
| 16:30       | 30 min       | tbd                                                                                                                                | tbd                                     |
| 17:00       | 30 min       | tbd                                                                                                                                | tbd                                     |
| **17:30** |              | **Meet and Greet**                                                                                                               | all                                     |
