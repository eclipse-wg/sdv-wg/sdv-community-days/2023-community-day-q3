Registration Instructions

1. Go to the Registration form. https://eclipsecon.regfox.com/eclipsecon-2023 

2. After filling in your personal information, select Community Day Only pass.

3. Enter the code K4CD32 in the Coupon Code box and click on “APPLY.”

4. Select your option under Community Day Choice.

5. Complete the rest of the registration form. Be sure to click on “REGISTER” at the end.

6. You will receive an email confirmation from the registration system after your registration is complete.

